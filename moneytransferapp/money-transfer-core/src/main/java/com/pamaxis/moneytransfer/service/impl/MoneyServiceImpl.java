package com.pamaxis.moneytransfer.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.jvnet.hk2.annotations.Service;

import com.pamaxis.moneytransfer.model.Account;
import com.pamaxis.moneytransfer.model.TransactionLine;
import com.pamaxis.moneytransfer.model.repository.AccountRepository;
import com.pamaxis.moneytransfer.service.AccountBalance;
import com.pamaxis.moneytransfer.service.MoneyService;

@Service
public class MoneyServiceImpl implements MoneyService {

    private Logger logger = Logger.getLogger(MoneyServiceImpl.class.getName());

    @Inject
    AccountRepository accountRepository;

    public boolean transferFunds(String sourceAccountNumber, String destinationAccountNumber, BigDecimal amount)
            throws InvalidAccountException, InsufficientFundsException, CurrencyMismatchException {

        if (amount == null || amount.longValue() == 0) {
            return false;
        }

        Account source = getAccount(sourceAccountNumber);
        Account destination = getAccount(destinationAccountNumber);

        if (!source.getCurrency().equalsIgnoreCase(destination.getCurrency())) {
            throw new CurrencyMismatchException(
                    "source and destination currencies MUST be the same to allow transfer of funds");
        }

        if (amount.longValue() > accountRepository.getAccountBalance(source).longValue()) {
            throw new InsufficientFundsException("the source account does not have enough funds to make the transfer");
        }
        transferFunds(source, destination, amount);
        return true;
    }

    public AccountBalance getAccountBalance(String accountNumber) throws InvalidAccountException {
        Account account = getAccount(accountNumber);
        BigDecimal accountBalance = accountRepository.getAccountBalance(account);

        return new AccountBalance(accountBalance, account.getCurrency());
    }

    @Transactional
    void transferFunds(Account source, Account destination, BigDecimal amount)
            throws CurrencyMismatchException, InsufficientFundsException {

        Date transactionTime = new Date();
        try {
            accountRepository.beginTransaction();
            addTransactionLine(source, destination.getAccountNumber(), amount.negate(), transactionTime);
            addTransactionLine(destination, source.getAccountNumber(), amount, transactionTime);
            accountRepository.commitTransaction();
        } catch (Exception e) {
            logger.info("Problem detected transfering funds: " + e.getMessage() + ".. rolling back transaction");
            accountRepository.rollbackTransaction();
            throw new RuntimeException(e);
        }
        return;
    }

    void addTransactionLine(Account account, String originOrDestination, BigDecimal amount, Date transactionTime) {

        TransactionLine transactionLine = new TransactionLine();
        transactionLine.setAccount(account);
        transactionLine.setAmount(amount);
        transactionLine.setOriginOrDestination(originOrDestination);
        transactionLine.setTransactionDate(transactionTime);

        account.getTransactionLines().add(transactionLine);

    }

    Account getAccount(String accountNumnber) throws InvalidAccountException {

        Account account = accountRepository.findAccountByAccountNumber(accountNumnber);
        if (account == null) {
            throw new InvalidAccountException(accountNumnber + " is not a valid account number!");
        }
        return account;
    }

}
