package com.pamaxis.moneytransfer.integrationtest;

import com.pamaxis.moneytransfer.rest.app.jersey.Application;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;
import static org.eclipse.jetty.http.HttpStatus.OK_200;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class MoneyTransferIntegrationTest extends JerseyTest {

    private Response response;

    @Override
    public Application configure() {
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new Application();
    }


    @Test
    public void shouldCorrectlyGetBlances() throws Exception {
        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/balance/5678").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/balance/7675").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"GBP\"}")));
    }

    @Test
    public void shouldCorrectlyHandleIncorrectAccountOnGetBlances() throws Exception {
        response = target("/balance/wrongaccount123").request().get();
        assertThat(response.getStatus(), is(equalTo(BAD_REQUEST_400)));
        assertThat(readEntityAsString(response), containsString("InvalidAccountException: wrongaccount123 is not a valid account number!"));
    }

    @Test
    public void shouldCorrectlyTransferFundsBetweenAccounts() throws Exception {
        response = target("/transfer/5678/1234/45.50").request().post(null);
        assertThat(response.getStatus(), is(equalTo(OK_200)));

        response = target("/balance/5678").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":54.50,\"currency\":\"USD\"}")));

        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":145.50,\"currency\":\"USD\"}")));

        response = target("/transfer/1234/5678/5.50").request().post(null);
        assertThat(response.getStatus(), is(equalTo(OK_200)));

        response = target("/balance/5678").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":60.00,\"currency\":\"USD\"}")));

        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":140.00,\"currency\":\"USD\"}")));

    }

    @Test
    public void shouldFailToTransferFundsBetweenAccountsWhenSourceAccountHasInsufficientFunds() throws Exception {
        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/balance/5678").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/transfer/5678/1234/102.00").request().post(null);
        assertThat(response.getStatus(), is(equalTo(BAD_REQUEST_400)));
        assertThat(readEntityAsString(response), containsString("InsufficientFundsException: the source account does not have enough funds to make the transfer"));

        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/balance/5678").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));
    }

    @Test
    public void shouldFailToTransferFundsBetweenAccountsWhenSourceAccountIsNotValid() throws Exception {
        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/transfer/432423/1234/10.00").request().post(null);
        assertThat(response.getStatus(), is(equalTo(BAD_REQUEST_400)));
        assertThat(readEntityAsString(response), containsString("InvalidAccountException: 432423 is not a valid account number!"));

        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));
    }

    @Test
    public void shouldFailToTransferFundsBetweenAccountsWhenDestinationAccountIsNotValid() throws Exception {
        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/transfer/1234/432423/10.00").request().post(null);
        assertThat(response.getStatus(), is(equalTo(BAD_REQUEST_400)));
        assertThat(readEntityAsString(response), containsString("InvalidAccountException: 432423 is not a valid account number!"));

        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));
    }

    @Test
    public void shouldFailToTransferFundsBetweenAccountsWhenSourceAndDestinationAccountsDontShareTheSameCurrency() throws Exception {
        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/balance/7675").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"GBP\"}")));

        response = target("/transfer/7675/1234/10").request().post(null);
        assertThat(response.getStatus(), is(equalTo(BAD_REQUEST_400)));
        assertThat(readEntityAsString(response), containsString("CurrencyMismatchException: source and destination currencies MUST be the same to allow transfer of funds"));

        response = target("/balance/1234").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"USD\"}")));

        response = target("/balance/7675").request().get();
        assertThat(response.getStatus(), is(equalTo(OK_200)));
        assertThat(readEntityAsString(response), is(equalTo("{\"amount\":100.00,\"currency\":\"GBP\"}")));
    }


    private String readEntityAsString(Response response) throws IOException {
        return IOUtils.toString((InputStream) response.getEntity(), Charset.defaultCharset());
    }

}
