package com.pamaxis.moneytransfer.rest.app.controller;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MoneyAppException extends Exception implements ExceptionMapper<MoneyAppException> {
    private static final long serialVersionUID = 1L;

    public MoneyAppException() {
        super();
    }

    public MoneyAppException(Throwable e) {
        super(e.getClass().getName() + ": " + e.getMessage());
    }

    @Override
    public Response toResponse(MoneyAppException exception) {
        return Response.status(400).entity(exception.getMessage())
                .type("text/plain").build();
    }
}