package com.pamaxis.moneytransfer.rest.app.controller;

import com.pamaxis.moneytransfer.persistence.DataImporter;
import com.pamaxis.moneytransfer.service.AccountBalance;
import com.pamaxis.moneytransfer.service.MoneyService;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

@Path("/")
@ManagedBean
public class MoneyRESTServiceController {

    private final MoneyService moneyService;
    private final DataImporter dataImporter;

    @Inject
    public MoneyRESTServiceController(MoneyService moneyService, DataImporter dataImporter) {
        this.moneyService = moneyService;
        this.dataImporter = dataImporter;
    }

    @POST
    @Path("transfer/{source}/{destination}/{amount}")
    @Produces(MediaType.TEXT_PLAIN)
    public boolean transferFunds(
            @PathParam("source") String sourceAccountNumber,
            @PathParam("destination") String destinationAccountNumber,
            @PathParam("amount") BigDecimal amount) throws MoneyAppException {

        try {
            return moneyService.transferFunds(sourceAccountNumber, destinationAccountNumber, amount);
        } catch (MoneyService.InvalidAccountException
                | MoneyService.InsufficientFundsException
                | MoneyService.CurrencyMismatchException e) {
            throw new MoneyAppException(e);
        }
    }

    @GET
    @Path("balance/{account}")
    @Produces(MediaType.APPLICATION_JSON)
    public AccountBalance getAccountBalance(@PathParam("account") String accountNumber) throws MoneyAppException {
        try {
            return moneyService.getAccountBalance(accountNumber);
        } catch (MoneyService.InvalidAccountException e) {
            throw new MoneyAppException(e);
        }
    }


}
