package com.pamaxis.moneytransfer.rest.app.jersey;

import net.sf.corn.cps.CPScanner;
import net.sf.corn.cps.ResourceFilter;
import org.glassfish.hk2.api.DescriptorFileFinder;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ClasspathDescriptorResourceFinder implements DescriptorFileFinder {
    private ClassLoader classLoader;

    public ClasspathDescriptorResourceFinder(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    public List<InputStream> findDescriptorFiles() throws IOException {

        List<InputStream> streams = new ArrayList<>();
        List<URL> resources = CPScanner.scanResources(new ResourceFilter().packageName("*hk2-locator*"));

        for (URL resource : resources) {
            if (resource.toString().endsWith("hk2")) {
                streams.add(resource.openStream());
            }
        }
        if (streams.isEmpty()) {
            throw new RuntimeException("unable to load hk2 binding files...Failing");
        }
        return streams;
    }
}
