package com.pamaxis.moneytransfer.rest.app.jersey;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.hibernate.annotations.common.util.impl.Log;

import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationPath("/moneyapp")
public class Application extends ResourceConfig {

    private final Logger logger = Logger.getLogger(Application.class.getName());

    public Application() {
        super();
        packages("com.pamaxis.moneytransfer");
        register(ComponentScanFeature.class);
        register(LoggingFeature.class);
        register(JacksonFeature.class);
        logger.setLevel(Level.ALL);
    }

}

