package com.pamaxis.moneytransfer.rest.app.jersey;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;

import org.glassfish.hk2.api.DynamicConfigurationService;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.Populator;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.DuplicatePostProcessor;
import org.glassfish.jersey.ServiceLocatorProvider;

public class ComponentScanFeature implements Feature {

    @Override
    public boolean configure(FeatureContext context) {
        ServiceLocator locator = ServiceLocatorProvider.getServiceLocator(context);
        DynamicConfigurationService dcs = locator.getService(DynamicConfigurationService.class);
        
        Populator populator = dcs.getPopulator();
        try {
            populator.populate(new ClasspathDescriptorResourceFinder(this.getClass().getClassLoader()),
                    new DuplicatePostProcessor());
        } catch (IOException | MultiException ex) {
            Logger.getLogger(ComponentScanFeature.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

}