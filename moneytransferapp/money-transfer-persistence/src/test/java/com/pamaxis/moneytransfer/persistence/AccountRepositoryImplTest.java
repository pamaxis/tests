package com.pamaxis.moneytransfer.persistence;

import com.pamaxis.moneytransfer.model.Account;
import com.pamaxis.moneytransfer.model.TransactionLine;
import com.pamaxis.moneytransfer.model.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class AccountRepositoryImplTest extends JpaBaseRolledBackTestCase{

    public static final String TEST_ACCOUNT_1_NAME = "test account 1";
    public static final String TEST_USER_1 = "test user 1";
    public static final String TEST_ACCOUNT_2_NAME = "test account 2";
    public static final String TEST_USER_2 = "test user 2";
    public static final String ACCOUNT_NUMBER_1 = UUID.randomUUID().toString();
    public static final String ACCOUNT_NUMBER_2 = UUID.randomUUID().toString();

    public static final String SOME_ORIGIN = "some origin";
    public static final String SOME_OTHER_USER = "some other user";
    public static final String ORIGIN_OR_DESTINATION = UUID.randomUUID().toString();

    private Account testAccount1 = new Account();
    private Account testAccount2 = new Account();

    private AccountRepositoryImpl accountRepositoryUnderTest;

    @Before
    public void setUp() throws Exception {
        accountRepositoryUnderTest = new AccountRepositoryImpl();
        accountRepositoryUnderTest.setEntityManager(entityManager);


        createAndPersistTestAccount(testAccount1, TEST_ACCOUNT_1_NAME, TEST_USER_1, ACCOUNT_NUMBER_1, BigDecimal.TEN);
        createAndPersistTestAccount(testAccount2, TEST_ACCOUNT_2_NAME, TEST_USER_2, ACCOUNT_NUMBER_2, new BigDecimal("55"));

    }

    private void createAndPersistTestAccount(Account testAccount, String testAccountName, String testUser, String accountNumber, BigDecimal initialAmount) {
        testAccount.setName(testAccountName);
        testAccount.setOwner(testUser);
        testAccount.setAccountNumber(accountNumber);

        TransactionLine transactionLine = new TransactionLine();
        transactionLine.setAmount(initialAmount);
        transactionLine.setOriginOrDestination(SOME_ORIGIN);
        transactionLine.setOriginOrDestination(SOME_OTHER_USER);
        transactionLine.setAccount(testAccount);

        testAccount.getTransactionLines().add(transactionLine);
        entityManager.persist(testAccount);
    }

    @Test
    public void shouldCorrectlyFindAccountById() throws Exception {
        //given

        //when
        Account accountById = accountRepositoryUnderTest.findAccountById(testAccount1.getId());

        //then
        assertThat(accountById, is(notNullValue()));
        assertThat(accountById.getName(), is(equalTo(testAccount1.getName())));
        assertThat(accountById.getOwner(), is(equalTo(testAccount1.getOwner())));
        assertThat(accountById.getTransactionLines(), hasSize(1));
        assertThat(accountById.getTransactionLines(), hasItem(testAccount1.getTransactionLines().get(0)));
    }

    @Test
    public void shouldCorrectlyFindAccountByOwner() throws Exception {
        //given

        //when
        Account accountByOwner = accountRepositoryUnderTest.findAccountByOwner(testAccount1.getOwner());

        //then
        assertThat(accountByOwner, is(notNullValue()));
        assertThat(accountByOwner.getName(), is(equalTo(testAccount1.getName())));
        assertThat(accountByOwner.getOwner(), is(equalTo(testAccount1.getOwner())));
        assertThat(accountByOwner.getTransactionLines(), hasSize(1));
        assertThat(accountByOwner.getTransactionLines(), hasItem(testAccount1.getTransactionLines().get(0)));
    }

    @Test
    public void shouldCorrectlyFindAccountByAccountNumber() throws Exception {
        //given

        //when
        Account accountByNumber = accountRepositoryUnderTest.findAccountByAccountNumber(testAccount1.getAccountNumber());

        //then
        assertThat(accountByNumber, is(notNullValue()));
        assertThat(accountByNumber.getName(), is(equalTo(testAccount1.getName())));
        assertThat(accountByNumber.getOwner(), is(equalTo(testAccount1.getOwner())));
        assertThat(accountByNumber.getAccountNumber(), is(equalTo(testAccount1.getAccountNumber())));
        assertThat(accountByNumber.getTransactionLines(), hasSize(1));
        assertThat(accountByNumber.getTransactionLines(), hasItem(testAccount1.getTransactionLines().get(0)));
    }

    @Test
    public void shouldCorrectlyReturnNullIfNoAccountByAccountNumber() throws Exception {
        //when
        Account accountByNumber = accountRepositoryUnderTest.findAccountByAccountNumber("wrong number");

        //then
        assertThat(accountByNumber, is(nullValue()));
    }

    @Test
    public void shouldCorrectlyReturnNullIfNoAccountByOwner() throws Exception {
        //when
        Account accountByNumber = accountRepositoryUnderTest.findAccountByOwner("wrong owner");

        //then
        assertThat(accountByNumber, is(nullValue()));
    }

    @Test
    public void shouldCorrectlyReturnNullIfNoAccountById() throws Exception {
        //when
        Account accountByNumber = accountRepositoryUnderTest.findAccountById(-1L);

        //then
        assertThat(accountByNumber, is(nullValue()));
    }

    @Test
    public void shouldCorrectlyReturnNullIfNullPassedToByAccountNumber() throws Exception {
        //when
        Account accountByNumber = accountRepositoryUnderTest.findAccountByAccountNumber(null);

        //then
        assertThat(accountByNumber, is(nullValue()));
    }

    @Test
    public void shouldCorrectlyReturnNullIfIfNullPassedToByOwner() throws Exception {
        //when
        Account accountByNumber = accountRepositoryUnderTest.findAccountByOwner(null);

        //then
        assertThat(accountByNumber, is(nullValue()));
    }

    @Test
    public void shouldCorrectlyReturnNullIfIfNullPassedToById() throws Exception {
        //when
        Account accountByNumber = accountRepositoryUnderTest.findAccountById(null);

        //then
        assertThat(accountByNumber, is(nullValue()));
    }

    @Test
    public void shouldCorrectlyPersistAdditionalTransactions() throws Exception {
        //given
        Account accountByOwner = accountRepositoryUnderTest.findAccountByOwner(testAccount1.getOwner());
        TransactionLine newTransactionLine = new TransactionLine();
        newTransactionLine.setAmount(new BigDecimal("-1"));
        newTransactionLine.setOriginOrDestination(ORIGIN_OR_DESTINATION);
        accountByOwner.getTransactionLines().add(newTransactionLine);

        //when
        Account account = accountRepositoryUnderTest.findAccountById(testAccount1.getId());

        //then
        assertThat(account, is(notNullValue()));
        assertThat(account.getName(), is(equalTo(testAccount1.getName())));
        assertThat(account.getOwner(), is(equalTo(testAccount1.getOwner())));
        assertThat(account.getTransactionLines(), hasSize(2));
        assertThat(account.getTransactionLines(), hasItem(testAccount1.getTransactionLines().get(0)));
        assertThat(account.getTransactionLines(), hasItem(testAccount1.getTransactionLines().get(1)));
    }

    @Test
    public void shouldCorrectlyGetAccountBalance() throws Exception {

        //given
        Account accountByOwner = accountRepositoryUnderTest.findAccountByOwner(testAccount1.getOwner());
        TransactionLine newTransactionLine = new TransactionLine();
        newTransactionLine.setAmount(new BigDecimal("-1"));
        newTransactionLine.setOriginOrDestination(ORIGIN_OR_DESTINATION);
        newTransactionLine.setAccount(testAccount1);
        accountByOwner.getTransactionLines().add(newTransactionLine);

        //when
        BigDecimal accountBalance = accountRepositoryUnderTest.getAccountBalance(testAccount1);

        //then
        assertThat(accountBalance, is(notNullValue()));
        assertThat(accountBalance, is(equalTo(new BigDecimal("9.00"))));

    }

    @Test
    public void shouldCorrectlyCreateNewAccount() throws Exception {

        //given
        String accountNumber = "fdsfldsf";
        String accountName = "s;alkdasldjal";
        String owner = "lsdha8ud83";
        String currency = "USD";
        BigDecimal amount = new BigDecimal(100000000);

        //when
        Account newAccount = accountRepositoryUnderTest.createNewAccount(accountNumber, accountName, owner, currency, amount);

        //then
        assertThat(newAccount, is(notNullValue()));
        assertThat(newAccount.getId(), is(notNullValue()));
        assertThat(newAccount.getAccountNumber(), is(equalTo(accountNumber)));
        assertThat(newAccount.getOwner(), is(equalTo(owner)));
        assertThat(newAccount.getCurrency(), is(equalTo(currency)));
        assertThat(newAccount.getTransactionLines(), hasSize(1));
        TransactionLine transactionLine = newAccount.getTransactionLines().get(0);
        assertThat(transactionLine.getAmount(), is(equalTo(amount)));
        assertThat(transactionLine.getId(), is(notNullValue()));
        assertThat(transactionLine.getOriginOrDestination(), is(equalTo(AccountRepository.DEFAULT_TRANSACTION_ORIGIN)));

    }
}

