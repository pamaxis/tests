package com.pamaxis.moneytransfer.persistence;

import com.pamaxis.moneytransfer.model.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DataImporterImplTest {

    @Mock
    private AccountRepository accountRepositoryMock;
    private DataImporterImpl dataImporterImplUnderTest;
    private ArgumentCaptor<String> accountNumber;
    private ArgumentCaptor<String> accountName;
    private ArgumentCaptor<String> accountOwner;
    private ArgumentCaptor<String> currency;
    private ArgumentCaptor<BigDecimal> initialAmount;

    @Before
    public void setUp() throws Exception {
        dataImporterImplUnderTest = new DataImporterImpl(accountRepositoryMock);

        accountNumber = ArgumentCaptor.forClass(String.class);
        accountName = ArgumentCaptor.forClass(String.class);
        accountOwner = ArgumentCaptor.forClass(String.class);
        currency = ArgumentCaptor.forClass(String.class);
        initialAmount = ArgumentCaptor.forClass(BigDecimal.class);

    }

    @Test
    public void shouldCorrectlyImportInitialData() throws Exception {
        //when
        dataImporterImplUnderTest.importInitialData();
        //then
        verify(accountRepositoryMock, times(3))
                .createNewAccount(
                        accountNumber.capture(),
                        accountName.capture(),
                        accountOwner.capture(),
                        currency.capture(),
                        initialAmount.capture());

        assertThat(accountNumber.getAllValues().get(0), is(equalTo("1234")));
        assertThat(accountName.getAllValues().get(0), is(equalTo("test1")));
        assertThat(accountOwner.getAllValues().get(0), is(equalTo("mr test1")));
        assertThat(currency.getAllValues().get(0), is(equalTo("USD")));
        assertThat(initialAmount.getAllValues().get(0), is(equalTo(new BigDecimal("1001"))));

        assertThat(accountNumber.getAllValues().get(1), is(equalTo("5678")));
        assertThat(accountName.getAllValues().get(1), is(equalTo("test2")));
        assertThat(accountOwner.getAllValues().get(1), is(equalTo("mr test2")));
        assertThat(currency.getAllValues().get(1), is(equalTo("USD")));
        assertThat(initialAmount.getAllValues().get(1), is(equalTo(new BigDecimal("1002"))));

        assertThat(accountNumber.getAllValues().get(2), is(equalTo("7675")));
        assertThat(accountName.getAllValues().get(2), is(equalTo("test3")));
        assertThat(accountOwner.getAllValues().get(2), is(equalTo("mr test3")));
        assertThat(currency.getAllValues().get(2), is(equalTo("GBP")));
        assertThat(initialAmount.getAllValues().get(2), is(equalTo(new BigDecimal("1003"))));
    }

}