package com.pamaxis.moneytransfer.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.jvnet.hk2.annotations.Service;

import com.pamaxis.moneytransfer.model.repository.AccountRepository;

@Service
public class DataImporterImpl implements DataImporter {

    public static final String INITIAL_DATA_CSV = "initial-data.csv";
    private final AccountRepository accountRepository;
    private Logger logger = Logger.getLogger(getClass().getName());

    @Inject
    public DataImporterImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    @PostConstruct
    public void importInitialData() {

        logger.info("Loading initial data...");
        int importCount = 0;
        CSVFormat format = CSVFormat.DEFAULT
                .withHeader(
                        "account number", "account name", "owner", "currency", "initial amount"
                ).withFirstRecordAsHeader()
                .withTrim();
        try {
            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(INITIAL_DATA_CSV);
            for (CSVRecord record : CSVParser.parse(inputStream, Charset.defaultCharset(), format)) {
                accountRepository.createNewAccount(
                        record.get("account number"),
                        record.get("account name"),
                        record.get("owner"),
                        record.get("currency"),
                        new BigDecimal(record.get("initial amount")));
                importCount++;
            }
        } catch (IOException e) {
            logger.info("Data import file " + INITIAL_DATA_CSV + " not found...");
        }

        logger.info("..." + importCount + " records imported");
    }

}
