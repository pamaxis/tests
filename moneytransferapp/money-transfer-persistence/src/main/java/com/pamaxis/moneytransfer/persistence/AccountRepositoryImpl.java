package com.pamaxis.moneytransfer.persistence;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.jvnet.hk2.annotations.Service;

import com.pamaxis.moneytransfer.model.Account;
import com.pamaxis.moneytransfer.model.TransactionLine;
import com.pamaxis.moneytransfer.model.repository.AccountRepository;

@Service
@Transactional
public class AccountRepositoryImpl implements AccountRepository {

    @PersistenceContext(unitName = "main")
    private EntityManager entityManager;

    public AccountRepositoryImpl() {
    }

    public Account findAccountById(Long accountId) {
        if (accountId == null) {
            return null;
        }
        return getEntityManager().find(Account.class, accountId);
    }

    public Account findAccountByOwner(String owner) {
        Query query = getEntityManager().createQuery("FROM Account account WHERE account.owner = :owner", Account.class);
        query.setParameter("owner", owner);
        for (Object o : query.getResultList()) {
            return (Account) o;
        }
        return null;
    }

    public Account findAccountByAccountNumber(String accountNumber) {
        Query query = getEntityManager().createQuery("FROM Account account WHERE account.accountNumber = :accountNumber", Account.class);
        query.setParameter("accountNumber", accountNumber);
        for (Object o : query.getResultList()) {
            return (Account) o;
        }
        return null;
    }

    public BigDecimal getAccountBalance(Account account) {
        Query query = getEntityManager().createQuery(
                "SELECT SUM(line.amount) FROM TransactionLine line " +
                        "WHERE line.account = :account", BigDecimal.class);
        query.setParameter("account", account);
        return (BigDecimal) query.getSingleResult();
    }

    public Account createNewAccount(String accountNumber, String accountName, String accountOwner, String currency, BigDecimal initialAmount) {
        beginTransaction();
        Account account = new Account();
        account.setAccountNumber(accountNumber);
        account.setOwner(accountOwner);
        account.setName(accountName);
        account.setCurrency(currency);
        TransactionLine transactionLine = new TransactionLine();
        transactionLine.setAccount(account);
        transactionLine.setAmount(initialAmount);
        transactionLine.setOriginOrDestination(DEFAULT_TRANSACTION_ORIGIN);
        account.getTransactionLines().add(transactionLine);

        getEntityManager().persist(account);
        commitTransaction();
        return account;
    }

    EntityManager getEntityManager() {
        if(entityManager == null){
            entityManager = Persistence.createEntityManagerFactory("main").createEntityManager();
        }
        return entityManager;
    }

    void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void beginTransaction() {
        EntityTransaction transaction = getEntityManager().getTransaction();
        if(!transaction.isActive()) {
            transaction.begin();
        }
    }

    public void commitTransaction() {
        getEntityManager().getTransaction().commit();
    }

    public void rollbackTransaction() {
        getEntityManager().getTransaction().rollback();
    }
}
