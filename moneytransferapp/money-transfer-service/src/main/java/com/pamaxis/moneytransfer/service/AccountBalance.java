package com.pamaxis.moneytransfer.service;

import java.math.BigDecimal;

public class AccountBalance {
    private final BigDecimal amount;
    private final String currency;

    public AccountBalance(BigDecimal amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

}
