package com.pamaxis.moneytransfer.service;

import java.math.BigDecimal;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface MoneyService {

    boolean transferFunds(String sourceAccountNumber,
                          String destinationAccountNumber,
                          BigDecimal amount)
            throws InvalidAccountException, InsufficientFundsException, CurrencyMismatchException;

    AccountBalance getAccountBalance(String accountNumber) throws InvalidAccountException;

    class CurrencyMismatchException extends Exception{
        public CurrencyMismatchException(String message) {
            super(message);
        }
    }

    class InsufficientFundsException extends Exception {
        public InsufficientFundsException(String message) {
            super(message);
        }
    }

    class InvalidAccountException extends Exception {
        public InvalidAccountException(String message) {
            super(message);
        }
    }
}
