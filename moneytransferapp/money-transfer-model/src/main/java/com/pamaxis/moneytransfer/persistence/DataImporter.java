package com.pamaxis.moneytransfer.persistence;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface DataImporter {
    void importInitialData();
}
