package com.pamaxis.moneytransfer.model.repository;

public interface Repository {

    void beginTransaction();

    void commitTransaction();

    void rollbackTransaction();
}
