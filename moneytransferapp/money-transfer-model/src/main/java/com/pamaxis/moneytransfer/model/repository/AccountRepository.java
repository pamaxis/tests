package com.pamaxis.moneytransfer.model.repository;

import java.math.BigDecimal;

import org.jvnet.hk2.annotations.Contract;

import com.pamaxis.moneytransfer.model.Account;

@Contract
public interface AccountRepository extends Repository {

    String DEFAULT_TRANSACTION_ORIGIN = "bank";

    Account findAccountById(Long accountId);

    Account findAccountByOwner(String owner);

    Account findAccountByAccountNumber(String accountNumber);

    BigDecimal getAccountBalance(Account account);

    Account createNewAccount(String accountNumber, String accountName, String accountOwner, String currency, BigDecimal initialAmount);



}
