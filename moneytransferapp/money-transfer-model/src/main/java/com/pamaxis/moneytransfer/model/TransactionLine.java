package com.pamaxis.moneytransfer.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class TransactionLine extends ModelBase{

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Account account;

    @Basic
    @Column(nullable = false)
    private String originOrDestination;

    @Basic
    @Column(nullable = false)
    private BigDecimal amount = BigDecimal.ZERO;

    @Basic
    @Column(updatable = false, nullable = false)
    private Date transactionDate = new Date();

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getOriginOrDestination() {
        return originOrDestination;
    }

    public void setOriginOrDestination(String originOrDestination) {
        this.originOrDestination = originOrDestination;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
