Money Transfer App by Pawel Szarlat

Welcome to a very simple RESTfull API to demonstrate the transfer of funds between accounts.

Instructions:
To Build, Test and Integration Test the App:

    moneytransferapp> mvn clean install

To Run the embedded jetty server on port 8080:

    moneytransferapp> mvn clean install -PrunTheApp

The app will deploy an in-memory DB and load it with 3 test accounts specified in the 'initial-data.csv' file.

    account number,account name, owner, currency, initial amount
    1234,test1,mr test1,USD,100
    5678,test2,mr test2,USD,100
    7675,test3,mr test3,GBP,100
 
The api details are as follows:

    GET    http://localhost:8080/balance/{account-number}

returns the balance and currency of the given account

    POST    http://localhost:8080/transfer/{source}/{destination}/{amount}

executes a transfer of funds between the account in a transaction



